package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ValidationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_400_status_when_use_null_para() throws Exception {
        mockMvc.perform(post("/api/valid")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"gender\":\"female\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_status_when_use_out_scop_para() throws Exception {
        mockMvc.perform(post("/api/valid")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"xiaoming\",\"gender\":\"female\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_status_when_use_not_null_para() throws Exception {
        mockMvc.perform(post("/api/valid")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"xiao\",\"gender\":\"female\"}"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("name").value("xiao"));
    }

    @Test
    void should_return_400_status_when_use_year_too_big() throws Exception {
        mockMvc.perform(post("/api/valid")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"xiao\",\"gender\":\"female\",\"yearOfBirth\":100000}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_200_status_when_use_year_in_1000_to_9999() throws Exception {
        mockMvc.perform(post("/api/valid")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"xiao\",\"gender\":\"female\",\"yearOfBirth\":2019}"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_400_status_use_unformat_email() throws Exception {
        mockMvc.perform(post("/api/valid")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"xiao\",\"email\":\"chdmazr\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_200_status_use_format_email() throws Exception {
        mockMvc.perform(post("/api/valid")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"xiao\",\"email\":\"chdmazr@gamil.com\"}"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("email").value("chdmazr@gamil.com"));
    }
}
