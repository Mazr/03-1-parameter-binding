package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;

class JacksonTest {
    @Test
    void should_serialize_object_to_json() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        String json = mapper.writeValueAsString(new SimpleClass());
        System.out.println(json);

        // { "dateTime": "2010-09-09T10:00:00Z" }
    }
}

class SimpleClass {
    private ZonedDateTime dateTime = ZonedDateTime.now(ZoneId.of("GMT"));

    public ZonedDateTime getDateTime() {
        return dateTime;
    }
}