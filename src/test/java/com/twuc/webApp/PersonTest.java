package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {
    @Test
    void should_get_json_using_jackson() throws JsonProcessingException {
        String name = "xiaoming";
        String gender = "male";
        Person person = new Person(name, gender);
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(person);
        System.out.println(json);
        assertEquals(json, "{\"gender\":\"male\",\"name\":\"xiaoming\"}");
    }

    @Test
    void should_revort_ser() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = "{\"gender\":\"male\",\"name\":\"xiaoming\"}" +
                "";
        Person person = mapper.readValue(json, Person.class);
        assertEquals(person.getGender(), "male");
    }
}