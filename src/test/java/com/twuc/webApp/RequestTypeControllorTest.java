package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class RequestTypeControllorTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_set_user_id_to_integer() throws Exception {
        mockMvc.perform(get("/api/users/20"))
                .andExpect(status().isOk())
                .andExpect(content().string("OK"));
    }

    @Test
    void should_return_OK_given_primitive_type() throws Exception {
        mockMvc.perform(get("/api/usersage/20"))
                .andExpect(status().isOk())
                .andExpect(content().string("OK"));
    }

    @Test
    void should_return_ok_byebye_when_has_two_var() throws Exception {
        mockMvc.perform(get("/api/users/20/books/8080"))
                .andExpect(status().isOk())
                .andExpect(content().string("OK,ByeBye"));
    }

    @Test
    void should_return_yes_when_url_has_paramer() throws Exception {
        mockMvc.perform(get("/api/users/10/books" +
                "?userName=xiaoming"))
                .andExpect(status().isOk())
                .andExpect(content().string("yes"));
    }

    @Test
    void should_return_hey_when_url_param_is_null() throws Exception {
        mockMvc.perform(get("/api/users/10/booktype"))
                .andExpect(status().isOk())
                .andExpect(content().string("hey, here you are!"));
    }

    @Test
    void should_return_accept_when_binging_successful() throws Exception {
        mockMvc.perform(get("/api/col").param("collection", "temp1", "temp2", "temp3"))
                .andExpect(status().isOk())
                .andExpect(content().string("Collection accepted"));
    }
}
