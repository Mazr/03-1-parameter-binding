package com.twuc.webApp;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class ValidationController {
    @PostMapping("/api/valid")
    public Person getPersonWithNull(@Valid @RequestBody Person person) {
        return person;
    }
}
