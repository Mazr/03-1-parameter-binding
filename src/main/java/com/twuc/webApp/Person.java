package com.twuc.webApp;


import javax.validation.constraints.*;

public class Person {
    private String gender;
    @Size(min = 2, max = 5)
    @NotNull
    private String name;

    public Person(String gender, @Size(min = 2, max = 5) @NotNull String name, @Max(9999) @Min(1000) Integer yearOfBirth) {
        this.gender = gender;
        this.name = name;
        this.yearOfBirth = yearOfBirth;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    @Max(9999)
    @Min(1000)
    private Integer yearOfBirth;

    public String getEmail() {
        return email;
    }

    @Email
    private String email;

    public String getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public Person() {
    }

    public Person(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }
}
