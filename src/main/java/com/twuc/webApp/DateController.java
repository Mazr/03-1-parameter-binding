package com.twuc.webApp;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZonedDateTime;

@RestController
public class DateController {
    private ZonedDateTime dateTime;

    @PostMapping("/api/datetimes")
    public MyDateTimeContract getDateTime(@RequestBody MyDateTimeContract dateTime) {
        return dateTime;
    }

    @PostMapping("/api/datetimes/sample")
    public MyDateTimeContract getDateTimeSample(@RequestBody MyDateTimeContract dateTime) {
        return dateTime;
    }
}
