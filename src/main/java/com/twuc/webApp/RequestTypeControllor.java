package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api")
public class RequestTypeControllor {
    @GetMapping("/users/{userId}")
    public String getUserIdType(@PathVariable Integer userId) {
        return "OK";
    }

    @GetMapping("/usersage/{userAge}")
    public String getUserNameType(@PathVariable int userAge) {
        return "OK";
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public String getUserIdandBookId(@PathVariable int userId, @PathVariable Integer bookId){
        return "OK,ByeBye";
    }

    @GetMapping("/users/{userId}/books")
    public String getBook(@PathVariable int userId, @RequestParam String userName){
        return "yes";
    }

    @GetMapping("/users/{bookId}/booktype")
    public String getBookTypeById(@PathVariable int bookId, @RequestParam(required = false,defaultValue = "???") String booktype){
        return "hey, here you are!";
    }

    @GetMapping("/col")
    public String getCollection(@RequestParam Collection<String> collection) {
        return "Collection accepted";
    }

}
